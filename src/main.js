import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import BootstrapVue from 'bootstrap-vue'
import { rutas } from './rutas.js'
import VueSelect from 'vue-select'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './assets/vue-select.css'

Vue.use(BootstrapVue);
Vue.use(VueRouter);
Vue.component('v-select', VueSelect)

export var bus = new Vue();

const enrutador = new VueRouter({
  mode: 'history',
  routes: rutas
});

var vm = new Vue({
  el: '#app',
  render: h => h(App),
  router: enrutador
});

