import axios from 'axios';
import urls from '../url';

export default {

  getAll() {
    let url = urls.data().server + '/talentfestapi/destinos';
    console.log(url);
    return axios.get(url);
  },

  getNombre(nombre) {
    let url = urls.data().server + '/talentfestapi/destinos';
    console.log(`${url}/${nombre}`);
    return axios.get(`${url}/${nombre}`);
  },

  postCotizacion(cotizacion){
    let url = urls.data().server + '/talentfestapi/cotizacion';

    console.log('POST destino: ' + cotizacion.destino);
    console.log('POST fecha_partida: ' + cotizacion.fecha_partida);
    console.log('POST fecha_retorno: ' + cotizacion.fecha_retorno);
    console.log('POST cantidad_pasajeros: ' + cotizacion.cantidad_pasajeros);

    let bodyFormData = new FormData();
    bodyFormData.set('destino', cotizacion.destino);
    bodyFormData.set('fecha_partida', cotizacion.fecha_partida);
    bodyFormData.set('fecha_retorno', cotizacion.fecha_retorno);
    bodyFormData.set('cantidad_pasajeros', cotizacion.cantidad_pasajeros);

    console.log(`${url}`);

    return axios.post(`${url}`,bodyFormData);
  }
}
