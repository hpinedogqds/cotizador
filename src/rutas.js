import InicioComponent from './componentes/mostrarFormulario'
import NotFound from './componentes/page404'

export const rutas = [

  {path:'', component: InicioComponent},
  {path:'*', component: NotFound}

];
